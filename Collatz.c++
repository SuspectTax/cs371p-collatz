// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------
/**
 * @file
 * @Contains primary algorithm functions for Collatz Conjecture evaluation
 */
// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <unordered_map>  // unordered map, cycle_cache

#include "Collatz.h"

using namespace std;

// Create global cycle_cache to store cycle lengths across function calls for
// easy access
unordered_map<long,long> cycle_cache;


// ------------
// collatz_read
// ------------

pair<long, long> collatz_read (const string& input_string) {
    istringstream sin(input_string);
    long range_beg;
    long range_end;
    sin >> range_beg >> range_end;
    return make_pair(range_beg, range_end);
}

// ------------
// collatz_eval
// ------------

/**
 * Determines the cycle length of a number n, and all numbers in its cycle path
 * @param n number you wish to find the cycle length of
 * @return cycle length of n
 */
long cycle_length (long n) {
    assert(n>0);
    // Check if number's cycle length is already in cache
    if(cycle_cache.find(n)==cycle_cache.end()) {
        // If not, place determine cycle length from other cycle lengths in cache
        cycle_cache.emplace(n, cycle_length((n%2)?(n*3+1):(n/2))+1);
    }
    assert(cycle_cache.at(n)>0);
    return cycle_cache.at(n);
}

long collatz_eval (long range_beg, long range_end) {
    // Determines starting and ending point of range, even if input numbers are swapped
    long start=min(range_beg,range_end);
    long stop=max(range_beg+1,range_end+1);
    assert(start>0);
    assert(stop<=1000000);
    // Set recursive base case for cycle length determination in cycle_length
    cycle_cache.emplace(1,1);
    // Updates lower bound of range as all numbers in the lower half of the
    // range of (1,stop) map to numbers in the upper half of the range
    start=max(start,stop/2);
    // Set the max cycle length to the lowest possible cycle length
    long max_cycle=1;
    // Iterate through all elements of the range or reduced range
    for(long k=start; k<stop; k++) {
        // Determine the cycle length of each element in range
        max_cycle=max(max_cycle,cycle_length(k));
    }
    assert (max_cycle>0);
    return max_cycle;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& write, long range_beg, long range_end, long collatz_result) {
    write << range_beg << " " << range_end << " " << collatz_result << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& read, ostream& write) {
    string input_string;
    while (getline(read, input_string)) {
        const pair<int, int> input_range = collatz_read(input_string);
        const long            range_beg = input_range.first;
        const long            range_end = input_range.second;
        const long            collatz_result = collatz_eval(range_beg, range_end);
        collatz_print(write, range_beg, range_end, collatz_result);
    }
}
