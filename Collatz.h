// ------------------------------
// projects/c++/collatz/Collatz.h
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------


/**
 * Reads the users input and separates it into readable ints for evaluation
 * @param input_string line of user input, i.e. "100 2915"
 * @return a pair of ints, representing the beginning and end of a range, [range_beg, range_end]
 */
pair<long, long> collatz_read (const string& input_string);

// ------------
// collatz_eval
// ------------

/**
 * Determines the maximum collatz conjecture cycle length of a range of numbers
 * @param range_beg the beginning of the range, inclusive
 * @param range_end the end       of the range, inclusive
 * @return the max cycle length of the range [range_beg, range_end]
 */
long collatz_eval (long range_beg, long range_end);

// -------------
// collatz_print
// -------------

/**
 * Prints out initial input string and result immediately following it
 * @param write an ostream
 * @param range_beg the beginning of the range, inclusive
 * @param range_end the end       of the range, inclusive
 * @param collatz_result the max cycle length
 */
void collatz_print (ostream& write, long range_beg, long range_end, long collatz_result);

// -------------
// collatz_solve
// -------------

/**
 * Handles primary algorithm input and output, where are we reading from and writing to
 * @param read an istream
 * @param write an ostream
 */
void collatz_solve (istream& read, ostream& write);

#endif // Collatz_h
